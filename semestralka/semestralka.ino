#include <SPI.h>
#include <Ethernet.h>
#include <SoftwareSerial.h>

const byte rxPin = 0;
const byte txPin = 1;

SoftwareSerial mySerial (rxPin, txPin);

byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};

IPAddress ip(10, 0, 0, 5);
IPAddress gateway(10, 0, 0, 1);
IPAddress _dns(8, 8, 8, 8);
IPAddress subnet(255, 255, 255, 0);
IPAddress server(10, 0, 0, 1);

EthernetClient client;

void setup() {
  //Serial.begin(9600);
  //while (!Serial) {
  //  ; // wait for serial port to connect. Needed for native USB port only
  //}
  //Serial.println("Waiting for port connection");
  delay(5000);
  //Serial.println("Waiting done");
  mySerial.begin(9600);

  // give the ethernet module time to boot up:
  delay(1000);

  Ethernet.begin(mac, ip, _dns, gateway, subnet);
  
  tryConnect();
}

bool tryConnect(){
   if (client.connect(server, 10001)) {
    client.println("connected");
    return true;
  } else {
    return false;
  }
}

String inString = String("Temp: ");
String buff[100];
int buffPos = 0;

void loop() {
  //temp read loop
  while (mySerial.available() > 0) {
    int inChar = mySerial.read();
    if (isDigit(inChar)) {
      inString += (char)inChar;
    }
    else if ((inChar == '\n' || inChar == '\r') && inString.length() > 6){
      if (client.connected()){
        client.println(inString);
        inString = String("Temp: ");
      }
      else{
        buff[buffPos++] = inString;
        if (buffPos > 99) buffPos = 0;
      }
    } else{
      inString = String("Temp: ");
      break;
    }
  }

  //reconnect when disconnected
  if (!client.connected()) {
    client.stop();
    delay(1000);
    while (!client.connected()){
      if(tryConnect()){
        for(int i = 0; i < buffPos; i++){
          client.println(buff[i]);
        }
        buffPos = 0;
        break;
      }
      else
        delay(1000);
    }
  }
}
