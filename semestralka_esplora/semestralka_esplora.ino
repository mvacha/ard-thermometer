#include <Esplora.h>
#include <SoftwareSerial.h>

const byte rxPin = 3;
const byte txPin = 11;

SoftwareSerial mySerial =  SoftwareSerial(rxPin, txPin);

void setup() {
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  mySerial.begin(9600); 
}

void loop() {
  int celsius = (Esplora.readTemperature(DEGREES_C));// - 32.0) * (5.0/9.0);
  String str(celsius);  
  mySerial.println(str);
  delay(1000);
}
